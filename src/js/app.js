$(function() {
    'use strict';

    $(document).ready(function() {

        // add phone function
        $('#addPhoneForm input').on('keyup', function() {
            var newPhone = $(this).val();
            if(newPhone.length == 18) {
                $(this).parents('form').find('.btn').prop('disabled', false)
            } else {
                $(this).parents('form').find('.btn').prop('disabled', true)
            }
        });
        $('#addPhoneForm').on('submit', function() {
            var newPhone = $(this).find('input').val();
            if(newPhone.length == 18) {
                $('#phone-input').append('<div class="field-line"><strong>'+newPhone+'</strong></div>');
                $(this).find('input').val('').parents('.modal').removeClass('active').find('.notification').removeClass('error, success'); // clear input, close modal end remove notification
            } else {
                $(this).parents('form').find('.btn').prop('disabled', true);
                $(this).find('.notification').addClass('error').text('Не верный формат номера телефона');
            }
            return false
        });


        // toggle header nav-bar
        $(document).on('click', '.mobile-nav-trigger', function() {
            $(this).toggleClass('active');
        });
        $(document).click(function(event){
            if(!$(event.target).closest('.nav-bar, .mobile-nav-trigger').length) {
                $('.mobile-nav-trigger').removeClass('active');
                event.stopPropagation();
            }
        });

        // form mask plugin
        $('input.phone').mask('+7 (999) 999-99-99');

        // show password btn
        $(document).on('click', '.show-pwd', function() {
            if($(this).hasClass('active')) {
                $(this).removeClass('active').prev('input').attr('type','password').focus();
            } else {
                $(this).addClass('active').prev('input').attr('type','text').focus();
            }
            return false;
        });

        // custom select
        var zIndex = $(document).find('select').length;
        $(document).find('select').each(function() {
            customSelect($(this), zIndex);
            $(this).hide();
            zIndex = zIndex - 1;
        });
        $(document).on('click', '.customSelect', function() {
            if(!$(this).hasClass('active')) {
                $(document).find('.customSelect').removeClass('active');
                $(this).addClass('active');
            } else {
                $(document).find('.customSelect').removeClass('active');
            }
            return false;
        });
        $(document).on('click', '.customSelect .item', function() {
            $(this).parents('.customSelect').removeClass('active').find('.item').removeClass('selected');
            $(this).addClass('selected').parents('.customSelect').find('.actual').removeClass('placeholder').attr('data-selected', $(this).attr('data-val')).focus().find('span').text($(this).text());
            $('#'+$(this).parents('.customSelect').attr('data-id')).val($(this).attr('data-val'));
            return false;
        });
        $(document).click(function(event){
            if(!$(event.target).closest('.customSelect').length) {
                $('.customSelect').removeClass('active');
                event.stopPropagation();
            }
        });

        // pop-up
        $(document).on('click', '.pop-up', function() {
            $('#'+$(this).attr('data-modal-id')).addClass('active');
            return false;
        });
        $(document).on('click', '.close-modal', function() {
            $(this).parents('.modal').removeClass('active');
            return false;
        });
    });

    function customSelect(elm, ZIndex) {
        var elmOptions = {
                id: elm.attr('id'),
                name: elm.attr('name'),
                class: (elm.attr('class')) ? ' '+elm.attr('class') : '',
                disabled: (elm.attr('disabled')) ? ' disabled' : '',
                placeholder: elm.attr('placeholder')
            },
            defaultValue = {
                value: '',
                title: ''
            },
            elmChilds = '', elmHtml = '', selected = '', phClass = '';
        elm.find('option').each(function() {
            if($(this).attr('selected')) {
                defaultValue.value = $(this).attr('value');
                defaultValue.title = $(this).text();
                selected = ($(this).attr('selected')) ? ' selected' : ''
            }
            elmChilds = elmChilds + '<a href="" class="item' + selected + '" data-val="' + $(this).attr('value') + '">' + $(this).text() + '</a>';
        });
        if(!defaultValue.title && elmOptions.placeholder) {
            phClass = ' placeholder';
            defaultValue.title = elmOptions.placeholder;
        } else if(!defaultValue.title && !elmOptions.placeholder) {
            defaultValue.title = elm.find(':first-child').text();
            defaultValue.value = elm.find(':first-child').attr('value');
        }
        elmHtml = '<div class="customSelect' + elmOptions.class + elmOptions.disabled + '" data-id="' + elmOptions.id + '" style="z-index:' + ZIndex + '">' +
            '<a href="" class="actual' + phClass + '" data-selected="' + defaultValue.value + '"><span>' + defaultValue.title + '</span></a>' +
            '<div class="_menu">' + elmChilds + '</div>' +
            '</div>';
        elm.after(elmHtml);
    }

});