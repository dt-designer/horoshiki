var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var cache = require('gulp-cache');
var inject = require('gulp-inject');
var del = require('del');
var browserSync = require('browser-sync');
var bowerFiles = require('main-bower-files');
var runSequence = require('run-sequence');
var bourbon = require('node-bourbon');
var notify = require("gulp-notify");
var plumber = require('gulp-plumber');


// Development Tasks
// -----------------

// Start browserSync server
gulp.task('browserSync', function() {
    browserSync({
        server: {
            baseDir: 'app',
            middleware: function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                next();
            }
        }
    })
});




// Copying assets files
gulp.task('dev.assets', function() {
    return gulp.src('src/assets/**/*')
        .pipe(gulp.dest('app/assets'))
        .pipe(browserSync.reload({
            stream: true
        }));
});


// Copying bower
gulp.task('dev.bower:copy', function() {
    return gulp.src(bowerFiles())
        .pipe(gulp.dest('app/bower'))
});

gulp.task('dev.sass', function() {
    return gulp.src('src/sass/**/*.sass')
        .pipe(sass({
            includePaths: bourbon.includePaths
        }).on("error", notify.onError()))
        .pipe(gulp.dest('app/css'));
});

gulp.task('dev.js', function () {
    gulp.src('src/js/**/*.js')
        .pipe(plumber())
        .pipe(gulp.dest('app/js'));
});

gulp.task('dev.html', function () {
    gulp.src('src/**/*.html')
        .pipe(gulp.dest('app'));
});

// Inject JS, CSS to HTML
gulp.task('dev.dependencies:inject', function() {
    function replacePath(filepath) {
        filepath = filepath.replace('/app/', '');
        return inject.transform.apply(inject.transform, arguments);
    }
    return gulp.src('./src/index.html')
        .pipe(inject(gulp.src(['./app/bower/*.js'], {read: false}), {
            starttag: '<!-- inject dependencies:js -->',
            endtag: '<!-- end dependencies:js -->',
            transform: replacePath
        }))
        .pipe(inject(gulp.src(['./app/bower/*.css'], {read: false}), {
            starttag: '<!-- inject dependencies:css -->',
            endtag: '<!-- end dependencies:css -->',
            transform: replacePath
        }))
        .pipe(inject(gulp.src(['./app/css/*.css'], {read: false}), {
            starttag: '<!-- inject project:css -->',
            endtag: '<!-- end project:css -->',
            transform: replacePath
        }))
        .pipe(inject(gulp.src(['./app/js/*.js'], {read: false}), {
            starttag: '<!-- inject project:js -->',
            endtag: '<!-- end project:js -->',
            transform: replacePath
        }))
        .pipe(gulp.dest('./app')
        .pipe(browserSync.reload({
            stream: true
        })));
});


// Watchers
gulp.task('watch', function() {
    gulp.watch('src/sass/**/*.sass', ['dev.sass', 'dev.dependencies:inject']);
    gulp.watch('src/js/**/*.js', ['dev.js', 'dev.dependencies:inject']);
    gulp.watch('src/assets/**/*', ['dev.assets', 'reload']);
    gulp.watch('src/**/*.html', ['dev.html', 'dev.dependencies:inject']);
});

gulp.task('dev', function(callback) {
    runSequence(
        ['dev.bower:copy', 'dev.sass', 'dev.js', 'dev.assets'],
        'dev.dependencies:inject',
        ['browserSync', 'watch'],
        callback
    )
});








// dist for production server
// --------------------------

// Copying assets files
gulp.task('dist.assets', function() {
    return gulp.src('src/assets/**/*')
        .pipe(gulp.dest('dist/assets'))
});

// Copying bower
gulp.task('dist.bower:copy', function() {
    return gulp.src(bowerFiles())
        .pipe(uglify())
        .pipe(gulp.dest('dist/bower'))
});

gulp.task('dist.sass', function() {
    return gulp.src('src/sass/**/*.sass')
        .pipe(sass({
            includePaths: bourbon.includePaths
        }))
        .pipe(cssnano())
        .pipe(gulp.dest('dist/css'));
});

gulp.task('dist.js', function () {
    gulp.src('src/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

// Inject JS, CSS to HTML
gulp.task('dist.dependencies:inject', function() {
    function replaceDistPath(filepath) {
        filepath = filepath.replace('/dist/', '');
        return inject.transform.apply(inject.transform, arguments);
    }
    return gulp.src('./src/index.html')
        .pipe(inject(gulp.src(['./dist/bower/*.js'], {read: false}), {
            starttag: '<!-- inject dependencies:js -->',
            endtag: '<!-- end dependencies:js -->',
            transform: replaceDistPath
        }))
        .pipe(inject(gulp.src(['./dist/bower/*.css'], {read: false}), {
            starttag: '<!-- inject dependencies:css -->',
            endtag: '<!-- end dependencies:css -->',
            transform: replaceDistPath
        }))
        .pipe(inject(gulp.src(['./dist/css/*.css'], {read: false}), {
            starttag: '<!-- inject project:css -->',
            endtag: '<!-- end project:css -->',
            transform: replaceDistPath
        }))
        .pipe(inject(gulp.src(['./dist/js/*.js'], {read: false}), {
            starttag: '<!-- inject project:js -->',
            endtag: '<!-- end project:js -->',
            transform: replaceDistPath
        }))
        .pipe(gulp.dest('./dist'))
});

// Cleaning
gulp.task('.dist.clean', function() {
    return del.sync('dist').then(function(cb) {
        return cache.clearAll(cb);
    });
});

gulp.task('dist.clean:dist', function() {
    return del.sync('dist/**/*');
});


// Build Sequences
// ---------------

gulp.task('dist', function(callback) {
    runSequence(
        'dist.clean:dist',
        ['dist.bower:copy', 'dist.sass', 'dist.js', 'dist.assets'],
        'dist.dependencies:inject',
        callback
    )
});